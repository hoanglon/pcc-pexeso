#include <iostream>

#include "pexeso.h"

//Functions for printing
void printMenu() {
    system("cls");
    std::cout << R"(

 .----------------.  .----------------.  .----------------.  .----------------.  .----------------.  .----------------.
| .--------------. || .--------------. || .--------------. || .--------------. || .--------------. || .--------------. |
| |   ______     | || |  _________   | || |  ____  ____  | || |  _________   | || |    _______   | || |     ____     | |
| |  |_   __ \   | || | |_   ___  |  | || | |_  _||_  _| | || | |_   ___  |  | || |   /  ___  |  | || |   .'    `.   | |
| |    | |__) |  | || |   | |_  \_|  | || |   \ \  / /   | || |   | |_  \_|  | || |  |  (__ \_|  | || |  /  .--.  \  | |
| |    |  ___/   | || |   |  _|  _   | || |    > `' <    | || |   |  _|  _   | || |   '.___`-.   | || |  | |    | |  | |
| |   _| |_      | || |  _| |___/ |  | || |  _/ /'`\ \_  | || |  _| |___/ |  | || |  |`\____) |  | || |  \  `--'  /  | |
| |  |_____|     | || | |_________|  | || | |____||____| | || | |_________|  | || |  |_______.'  | || |   `.____.'   | |
| |              | || |              | || |              | || |              | || |              | || |              | |
| '--------------' || '--------------' || '--------------' || '--------------' || '--------------' || '--------------' |
 '----------------'  '----------------'  '----------------'  '----------------'  '----------------'  '----------------'

        )" << std::endl;


    std::cout << R"(
                                             Welcome to the game PEXESO!

                                                      MENU:

                                                 S - Start Game

                                                    H - Help

                                             Q(or anyother key) - QUIT
)";

}

void printHelp() {
    std::cout << R"(
    +-------------------------------------------------------------------+
    |                       Help menu for PEXESO                        |
    | After each argument press Enter to select your choice.            |
    |                                                                   |
    | RULES:                                                            |
    | The aim of the game is to find all pairs of letters.              |
    | Choose a card to turn around by writing its coordinates(EQ "B2")  |
    | After finding 2 of the same cards, they disappear and cannot be   |
    | selected again.                                                   |
    | You can't choose coordinates that are off the board.              |
    | You win by finding all the pairs and then typing CONTINUE.        |
    | You can turn off the game with pressing 0(and Enter).             |
    +-------------------------------------------------------------------+)" << std::endl;


}

void printYouWon() {
    std::cout << R"(


 __     ______  _    _      __          ______  _   _
 \ \   / / __ \| |  | |     \ \        / / __ \| \ | |
  \ \_/ / |  | | |  | |      \ \  /\  / / |  | |  \| |
   \   /| |  | | |  | |       \ \/  \/ /| |  | | . ` |
    | | | |__| | |__| |        \  /\  / | |__| | |\  |
    |_|  \____/ \____/          \/  \/   \____/|_| \_|


)" << std::endl;

}

int askForPairs() {
    int pairs;
    std::cout << "How many pairs of cards would you want in the game(1 - 26)?\n";
    std::cin >> pairs;
    // Ensure the input number is within the valid range
    if (pairs < 1 || pairs > 26) {
        std::cout << "Input number should be between 1 and 26." << std::endl;
        askForPairs();
    }
    return pairs;
}


int main(int argc, char *argv[]) {

    for (int i = 0; i < argc; ++i) {
        if (std::string(argv[i]) == "--help") {
            system("cls");
            printHelp();
            char start;
            std::cout << "Write 'M' to go to menu\n";
            std::cout << "Write any other letter to quit the game\n";
            std::cin >> start;
            if (toupper(start) == 'M') {
                continue;
            } else {
                std::cout << "GAME ENDED" << std::endl;
                return 0;
            }
        }
    }


    printMenu();
    char userInput;
    std::cin >> userInput;

    switch (toupper(userInput)) {
        case 'S': {
            int pair = askForPairs();
            Pexeso game(pair);
            break;
        }
        case 'H':
            printHelp();
            char start;
            std::cout << "Write 'S' to start the game\n";
            std::cout << "Write any other letter to quit the game\n";
            std::cin >> start;

            if (toupper(start) == 'S') {
                {
                    int pair = askForPairs();
                    Pexeso game(pair);
                    break;
                }
            } else {
                std::cout << "GAME ENDED" << std::endl;
                return 0;
            }
            break;
        case 'Q':
            system("cls");
            std::cout << "GAME ENDED";
            return 0;

    }
    bool gameWin = Pexeso::getGameWin();

    bool gameOver = Pexeso::getGameover();

    if (gameWin) {
        printYouWon();
        std::vector<long long> timesScored = Pexeso::getTimeScored();
        for (int i = 0; i < timesScored.size(); i++) {
            std::cout << i + 1 << ". pair found in " << timesScored[i] / 1000 << "sec" << std::endl;
        }
    } else if (gameOver) {
        std::cout << std::endl;
        std::cout << "HRA BYLA UKONČENA" << std::endl;
    }


    return 0;
}
