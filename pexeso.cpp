
#include <mutex>
#include <algorithm>
#include <random>
#include <thread>
#include "pexeso.h"


Pexeso::Pexeso(int Pairs) {
    //Calculate how many cols and rows are needed
    totalCards = Pairs;
    int result = Pairs * 2;

    cols = 1;
    rows = result;

    for (int i = 1; i <= std::sqrt(result); ++i) {
        if (result % i == 0) {
            cols = i;
            rows = result / i;
        }
    }
    score = 0;

    addCard();
    renderBoard();

    startThreads();
}

static bool gameover = false;
static bool gameWin = false;
static std::vector<long long> timeScored;

Pexeso::~Pexeso() {
    board.clear();
};

void Pexeso::startThreads() {
    std::thread computingThread(&Pexeso::computingThread, this);
    std::thread outputThread(&Pexeso::outputThread, this);
    std::thread inputThread(&Pexeso::inputThread, this);

    inputThread.join();
    computingThread.join();
    outputThread.join();
}

void Pexeso::inputThread() {
    std::string input;
    while (true) {
        if (gameover) {
            break;
        }

        if (gameWin) {
            break;
        }
        bool valid = false; // Checks for invalid inputs
        std::pair<int, int> coordsInput;
        while (!valid) {
            std::cin >> input;
            if (input == "CONTINUE") {
                break;
            }
            if (gameWin) {
                break;
            }
            if (input == "0") {
                system("cls");
                gameover = true;
                break;
            }
            coordsInput = convertToCoords(input);
            if (coordsInput.first != -1) { valid = true; }
        }
        inputCoords = coordsInput;
        inputReady = true;
        inputCV.notify_all();
    }
}

void Pexeso::outputThread() {
    while (true) {
        if (gameWin) {
            break;
        }
        if (gameover) {
            break;
        }
        std::unique_lock<std::mutex> lock(boardMutex);
        inputCV.wait(lock, [this] { return inputReady || gameWin; });

        logic(inputCoords);
        inputReady = false;
        logicReady = true;
        renderBoard();
        if (gameWin) {
            std::cout << std::endl << "type CONTINUE to continue to end screen" << std::endl;
            break;
        }

        inputCV.notify_all();

    }
}

void Pexeso::computingThread() {
    auto start_time = std::chrono::high_resolution_clock::now();
    while (true) {
        if (gameover) {
            break;
        }

        if (gameWin) {
            break;
        }

        std::unique_lock<std::mutex> lock(boardMutex);
        inputCV.wait(lock, [this] { return logicReady || gameWin; });


        if (scored) { //Checks for scored pair to add into vertex
            auto end_time = std::chrono::high_resolution_clock::now();
            auto elapsed_time = std::chrono::duration_cast<std::chrono::milliseconds>(end_time - start_time);
            timeScored.push_back(elapsed_time.count());
            scored = false;
        }
        logicReady = false;
        if (gameWin) {
            break;
        }
        // Notify the output thread that computation is done
        inputCV.notify_all();
    }
}

void Pexeso::addCard() {

    std::vector<Card> tmp;// Creates temporary deck of cards, which is then added to board randomly
    for (int i = 0; i < totalCards; i++) {
        Card card;
        card.value = 'A' + i;
        tmp.push_back(card);

        Card card2;
        card2.value = 'A' + i;
        tmp.push_back(card2);
    }

    std::default_random_engine rng(std::random_device{}());
    std::shuffle(tmp.begin(), tmp.end(), rng);
    for (int k = 0; k < rows; k++) {
        std::vector<Card> tmp2;
        board.push_back(tmp2);
        for (int j = 0; j < cols; j++) {
            board[k].push_back(tmp.back());
            tmp.pop_back(); // Remove the last card from tmp
//            std::cout << board[k][j].value << " "; // SHOW ANSWERS :D
        }
//        std::cout << std::endl;
    }
    tmp.clear();// Deletes tmp
}

void Pexeso::renderBoard() { // Function for drawing board and cards
    if (!gameover) {
        std::cout << "\t |";
        for (int i = 0; i < cols; i++) {
            std::cout << "\t" << i + 1;
        }
        std::cout << std::endl;
        for (int i = 0; i < cols; i++) {
            std::cout << "-------------";
        }
        std::cout << std::endl;
        for (int i = 0; i < rows; i++) {
            char rowLetter = 'A' + i;
            std::cout << "    " << rowLetter << "    |";

            for (int j = 0; j < cols; j++) {
                if (board[i][j].collected) {
                    std::cout << "\t ";
                } else if (board[i][j].showed) {
                    std::cout << "\t" << board[i][j].value;
                } else {
                    std::cout << "\t0";
                }

            }

            std::cout << std::endl;
        }
    }
}

std::pair<int, int> Pexeso::convertToCoords(const std::string input) {

    // Check if long enough
    if (input.size() < 2) {
        std::cout << "Wrong coordinates";
        return {-1, -1};
    }
    char letter = std::toupper(input.at(0));

    //Converts input into rows and cols(number)
    int number = input[1] - 49;
    int row = letter - 'A';

    // Check for out of bound
    if (row < 0 || row + 1 > rows || number < 0 || number + 1 > cols) {
        std::cout << "Wrong coordinates";
        return {-1, -1};
    }

    return {row, number};

}

void Pexeso::logic(std::pair<int, int> Coords) {
    int y = Coords.first;
    int x = Coords.second;
    //Check for already paired
    if (board[y][x].collected) {
        std::cout << "invalid input\n";
    } else if (board[y][x].showed && showedCards.size() < 2) { // Check for showing cards
        std::cout << "already showing\n";
    } else {
        board[y][x].showed = true;
        showedCards.push_back(&board[y][x]);
    }
    if (showedCards.size() == 2) { // Adds to collected cards
        if (showedCards[0]->value == showedCards[1]->value) {
            showedCards[0]->collected = true;
            showedCards[1]->collected = true;
            score++;
            scored = true;
            showedCards.clear();
            if (score == totalCards) {
                gameWin = true;
            }
        }
    } else if (showedCards.size() == 3) { //Remove showed cards
        showedCards[0]->showed = false;
        showedCards[0] = showedCards[2];
        showedCards[1]->showed = false;
        showedCards[0]->showed = true;
        showedCards.pop_back();
        showedCards.pop_back();

    }

}

bool Pexeso::getGameWin() {
    return gameWin;
};

bool Pexeso::getGameover() {
    return gameover;
}

std::vector<long long> Pexeso::getTimeScored() {
    return timeScored;
}
