# Hra PEXESO

## Popis hry

Cílem hry je najít všechny páry karet, které jsou náhodně rozprostřené na herním poli. Karty se vybírají pomocí souřadnic ve formátu "PísmenoČíslo" (např "a1", "B3"), které jsou dostupné na poli. Na konci hry jsou vypsané časy, za jak dlouho byly každý páry karet nalezeny.

Hra je implementována pomocí 3 vláken. První vlákno je zodpovědné za zobrazení samotné hry, druhé vlákno se stará o příjem vstupu od hráče a třetí vlákno měří čas, za jak dlouho se našly páry karet.

Hra může být kdykoliv ukončena klávesou '0'.

## Kompilace programu
```bash
cmake -Bcmake-build-debug -H. -DCMAKE_BUILD_TYPE=Debug
cmake --build cmake-build-debug
```

## Příklady spuštění programu
### Spustit hru
```bash
./cmake-build-debug/semPCC
```
### Zobrazit nápovědu
```bash
./cmake-build-debug/semPCC --help
```
Po zobrazení pravidel, bude hráč dotázán, zda-li chce spustit či ukončit hru.
```
    +-------------------------------------------------------------------+
    |                       Help menu for PEXESO                        |
    | After each argument press Enter to select your choice.            |
    |                                                                   |
    | RULES:                                                            |
    | The aim of the game is to find all pairs of letters.              |
    | Choose a card to turn around by writing its coordinates(EQ "B2")  |
    | After finding 2 of the same cards, they disappear and cannot be   |
    | selected again.                                                   |
    | You can't choose coordinates that are off the board.              |
    | You win by finding all the pairs and then typing CONTINUE.        |
    | You can turn off the game with pressing 0(and Enter).             |
    +-------------------------------------------------------------------+

```

## Testování
### Technika

Hra byla testována kompletně `ručně`, které díky rozdělení každé malé části do určitých funkcí bylo velmi přívětivé. Testováno bylo zda-li se opravdu po zadání všech správných písmen či vypršení herního času hra opravdu ukončí.

### Nástroje

Převážně jsem používal nástroj `Dr.Memory` pro detekci paměťových chyb a úniků paměti.

## Nedostatky
- Při použití `system("cls")` se terminál nevymaže, ale pouze posune

## Finální slovo
Implementace hry mě velmi bavila, i přes občasné technické problémy. Největší problém mi dělala implementace vláken a jejich synchorizace. Celkově bych hodnotil celý předmět velmi kladně.

Doufám, že výsledný program splňuje všechny požadavky a těším se na zpětnou vazbu. 
