//
// Created by w3sw3 on 13/01/2024.
//

#ifndef SEMCPP_PEXESO_H
#define SEMCPP_PEXESO_H

#include <iostream>
#include <vector>
#include <condition_variable>

struct Card {
    bool collected = false;//TRUE if card has found its pair
    bool showed = false;// TRUE if its being showed on the board
    char value;// VAlUE to compare cards
};


class Pexeso {
    int rows;
    int cols;
    int totalCards;
    std::vector<std::vector<Card>> board;// ALL the cards
    std::mutex boardMutex;  // Mutex to protect access to the board
    std::condition_variable inputCV;  // Condition variable for signaling input
    bool inputReady = false;  // Flag indicating whether input is ready
    bool logicReady = false;// Flag indicating whether output is ready
    std::vector<Card *> showedCards; // Keep track of showing cards in order to not show too many
    std::pair<int, int> inputCoords; // Coordinates that are being examined
    int score; // Keep score about the game
    bool scored = false; // TRUE if pair is found


public:
    Pexeso(int Pairs);

    ~Pexeso();

    void addCard(); // Adds all needed cards based on totalCards

    void startThreads(); // Adds 3 threads

    void inputThread();

    void computingThread();

    void outputThread();

    void renderBoard(); // Graphics of the game

    void logic(std::pair<int, int> Coords); // Logic about the attributes of cards -> changes how they render

    std::pair<int, int> convertToCoords(const std::string input); // Example: Converts "a1" to 00 on board

    static bool getGameWin();

    static bool getGameover();

    static std::vector<long long> getTimeScored();
};

#endif //SEMCPP_PEXESO_H
